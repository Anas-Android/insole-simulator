package com.duster.fr.feetmeinsole.controllers;

import android.bluetooth.BluetoothDevice;

/**
 * Created by Anas on 20/02/2016.
 */
public interface IRemoteDeviceController {

    /**
     * Connection state is disconnected.
     */
    int STATE_DISCONNECTED = 1;

    /**
     * Connection state is disconnected.
     */
    int STATE_LISTEN = 2;

    /**
     * Initiating an outgoing connection.
     */
    int STATE_CONNECTING = 3;

    /**
     * Connected to a remote device
     */
    int STATE_CONNECTED = 4;

    /**
     * Get the connection state.
     *
     * @return int
     */
    int getState();

    /**
     * Accept remote devices' connection requests.
     */
    void accept();

    /**
     * Stops the connection.
     */
    void disconnect();

    /**
     * send first frame after connection.
     */
    void sendFirstFrame();

    interface Listener {

        /**
         * Notification that the state of the connection with the distant device has changed.
         *
         * @param state
         */
        void onStateChanged(int state);

        /**
         * The battery level of the insole is requested
         */
        void onBatteryRequested();

        /**
         * The version of the insole is requested.
         */
        void onVersionRequested();

        /**
         * The number of sensors of the insole is received.
         */
        void onSensorNbRequested();

        /**
         * The size of the insole is requested.
         */
        void onSizeRequested();

        /**
         * The side of the insole is received.
         */
        void onSideRequested();

        /**
         * The response to ping has been requested.
         */
        void onPongRequested();

        /**
         * The timestamp of the insole has been reset.
         */
        void onResetTimestampRequested();

        /**
         * The insole will start sending data.
         */
        void onStartSending();

        /**
         * The insole will stop sending data.
         */
        void onStopSending();

        /**
         * The insole has just calibrated itself.
         */
        void onCalibration();

        void onRemoteDeviceConnected(BluetoothDevice device);
    }
}
