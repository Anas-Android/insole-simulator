package com.duster.fr.feetmeinsole.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.duster.fr.feetmeinsole.R;
import com.duster.fr.feetmeinsole.greendao.Insole;
import com.duster.fr.feetmeinsole.utils.PreferenceUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Anas on 29/02/2016.
 */
public class InsoleAdapter extends ArrayAdapter<Insole> {

    private List<Insole> mInsoles;
    private Insole mDefaultInsole;
    private Set<String> mAddresses = new HashSet<>();
    private boolean isInsoleConnected;
    private Context mContext;

    public InsoleAdapter(Context context, List<Insole> insoles) {
        super(context, R.layout.layout_insoles, insoles);

        mInsoles = insoles;
        mContext = context;

        for(Insole insole: insoles){
            if(insole != null && insole.getAddress() != null) {
                mAddresses.add(insole.getAddress());
            }
        }

        mDefaultInsole = PreferenceUtils.getDefaultInsole(context);

    }

    public InsoleAdapter(Context context){
        this(context, new ArrayList<Insole>());
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        InsoleViewHolder insoleViewHolder;

        if(convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.layout_insoles, null);
            insoleViewHolder = new InsoleViewHolder();

            insoleViewHolder.init(convertView);
            convertView.setTag(insoleViewHolder);
        }else{
            insoleViewHolder = (InsoleViewHolder) convertView.getTag();
            insoleViewHolder.init(convertView);
        }

        final Insole insole = mInsoles.get(position);
        insoleViewHolder.populate(insole);

        return convertView;
    }

    public void setDefaultInsole(Insole insole){
        mDefaultInsole = insole;
    }

    public void sort(){
        List<Insole> insoles = new ArrayList<>(mInsoles);
        mInsoles.clear();

        //add to first position the default insole if it is in the list
        if(mDefaultInsole != null){
            String defaultAddress = mDefaultInsole.getAddress();
            if(mAddresses.contains(defaultAddress)) {
                for (Insole insole : insoles) {
                    if (insole.getAddress().equals(defaultAddress)) {
                        mInsoles.add(insole);
                        insoles.remove(insole);
                        break;
                    }
                }
            }
        }

        Collections.sort(insoles, new Comparator<Insole>() {
            @Override
            public int compare(Insole lhs, Insole rhs) {
                return lhs.getSize() - rhs.getSize();
            }
        });

        mInsoles.addAll(insoles);
    }

    public void setInsoleConnected(boolean isInsoleConnected) {
        this.isInsoleConnected = isInsoleConnected;
    }

    @Override
    public void add(Insole insole) {
        String address = insole.getAddress();
        if(!mAddresses.contains(address)){
            String name = insole.getName();
            if(name != null && !name.equals("")){
                super.add(insole);
                mAddresses.add(address);
            }
        }
    }

    @Override
    public void addAll(Collection<? extends Insole> insoles) {
        for(Insole insole: insoles){
            add(insole);
        }
    }

    @Override
    public void remove(Insole insole) {
        super.remove(insole);
        mAddresses.remove(insole.getAddress());
    }

    @Override
    public void clear() {
        super.clear();
        mAddresses.clear();
    }

    class InsoleViewHolder {
        TextView name;
        TextView address;
        ImageView state;

        void init(View convertView){
            this.name = (TextView) convertView.findViewById(R.id.item_insole_name);
            this.state = (ImageView) convertView.findViewById(R.id.ic_state);
            this.address = (TextView) convertView.findViewById(R.id.item_insole_address);
        }

        void populate(final Insole insole){
            if(insole == null){
                return;
            }

            if(mDefaultInsole != null){
                if(insole.getAddress().equals(mDefaultInsole.getAddress())){
                    Drawable drawable;
                    PorterDuffColorFilter filter;
                    if(isInsoleConnected){
                        drawable = ContextCompat.getDrawable(mContext,R.drawable.vector_drawable_check);
                        filter = new PorterDuffColorFilter(ContextCompat.getColor(mContext,android.R.color.holo_green_light), PorterDuff.Mode.MULTIPLY);
                    }else{
                        drawable = ContextCompat.getDrawable(mContext, R.drawable.vector_drawable_circle);
                        filter = new PorterDuffColorFilter(ContextCompat.getColor(mContext,R.color.colorAccent), PorterDuff.Mode.MULTIPLY);
                    }
                    drawable.setColorFilter(filter);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        state.setBackground(drawable);
                    }
                }
            }

            name.setText(insole.getName());
            address.setText(insole.getAddress());
        }
    }
}
