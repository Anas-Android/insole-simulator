package com.duster.fr.feetmeinsole.views;

import android.content.Context;
import android.support.v7.widget.AppCompatSpinner;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import com.duster.fr.feetmeinsole.R;
import com.duster.fr.feetmeinsole.utils.PreferenceUtils;

/**
 * Created by Anas on 02/03/2016.
 */
public class SettingsView extends LinearLayout {

    private AppCompatSpinner mTypeSpinner;
    private AppCompatSpinner mFrequencySpinner;

    public SettingsView(Context context) {
        super(context);
        init(context);
    }

    public SettingsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.layout_settings, this, true);

        //--- Type ---//
        mTypeSpinner = (AppCompatSpinner) findViewById(R.id.type_spinner);
        String[] typeItems = new String[3];
        for(int i = 0; i < typeItems.length; i++){
            typeItems[i] = String.valueOf(i+1);
        }
        ArrayAdapter<String> typeAdapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, typeItems);
        mTypeSpinner.setAdapter(typeAdapter);

        //--- Frequency ---//
        mFrequencySpinner = (AppCompatSpinner) findViewById(R.id.frequency_spinner);
        String[] frequencyItems = new String[10];
        for(int i=0; i<frequencyItems.length; i++){
            frequencyItems[i] = String.valueOf((i+1)*10);
        }
        ArrayAdapter<String> frequencyAdapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, frequencyItems);
        mFrequencySpinner.setAdapter(frequencyAdapter);

        initSettings();
    }

    public void updateSettings(){
        PreferenceUtils.setDefaultFrequency(getContext(), (mFrequencySpinner.getSelectedItemPosition()+1)*10);
        PreferenceUtils.setDefaultDataType(getContext(), mTypeSpinner.getSelectedItemPosition() + 1);

    }


    private void initSettings() {
        int defaultFrequency = PreferenceUtils.getDefaultFrequency(getContext());
        if(defaultFrequency == 0){
            mFrequencySpinner.setSelection(9);
            PreferenceUtils.setDefaultFrequency(getContext(), 100);
        }else{
            mFrequencySpinner.setSelection((defaultFrequency-1)/10);
        }

        int defaultType = PreferenceUtils.getDefaultDataType(getContext());
        if(defaultType == 0){
            mTypeSpinner.setSelection(0);
            PreferenceUtils.setDefaultFrequency(getContext(), 1);
        }else{
            mTypeSpinner.setSelection(defaultType-1);
        }
    }
}
