package com.duster.fr.feetmeinsole.service;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;

import com.duster.fr.feetmeinsole.Build;
import com.duster.fr.feetmeinsole.greendao.Insole;
import com.duster.fr.feetmeinsole.managers.IInsoleManager;
import com.duster.fr.feetmeinsole.managers.InsoleManager;
import com.duster.fr.feetmeinsole.notifications.BluetoothNotificationManager;
import com.duster.fr.feetmeinsole.notifications.DisconnectionNotification;
import com.duster.fr.feetmeinsole.utils.InsoleUtils;
import com.duster.fr.feetmeinsole.utils.PreferenceUtils;


/**
 * Created by Anas on 19/02/2016.
 */
public class MainService extends Service implements IMainServiceManager{

    private static final String TAG = MainService.class.getSimpleName();

    private static final String PREF_CONNECTION_STARTED = "service_status";
    private IInsoleManager mManager;
    private Insole mInsole;
    private volatile boolean isConnectionStarted;

    private BroadcastReceiver mBluetoothStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(intent.getAction())) {
                if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1)
                        == BluetoothAdapter.STATE_OFF){
                    onBluetoothOff(context);
                }
            }
        }
    };

    private Binder mBinder = new MainBinder();

    public class MainBinder extends Binder {
        public IMainServiceManager getMainManager(){
            return MainService.this;
        }
    }

    protected IInsoleManager newInsoleManager(Context context, Insole insole){
        return new InsoleManager.Builder(context, insole)
                .setDiscNotificationManager(new DisconnectionNotification(context))
                .build();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Context context = getBaseContext();
        mInsole = PreferenceUtils.getDefaultInsole(context);
        isConnectionStarted = false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(Build.DEBUG) Log.i(TAG, "Service started");

        if(isConnectionPreferenceOn()){
            if(BluetoothAdapter.getDefaultAdapter().isEnabled()){
                if(!isConnectionStarted){
                    startConnection();
                }else {
                    new BluetoothNotificationManager(getBaseContext()).sendNotification();
                }
            }
        }
        return START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy() {
        stopConnection();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public synchronized void startConnection() {
        if(Build.DEBUG) Log.d(TAG, "startConnection");
        isConnectionStarted = true;
        setConnectionPreference(true);

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(mBluetoothStateReceiver, filter);

        if(mManager == null && mInsole != null){
            mManager = newInsoleManager(getBaseContext(), mInsole);
            mManager.startAcceptingConnections();
            if(Build.DEBUG) Log.d("InsoleManager", "startAccepting");
            if(Build.DEBUG) Log.d("Insole", "is null?" + (mInsole == null));
        }
    }

    protected void onBluetoothOff(Context context){
        new BluetoothNotificationManager(context).sendNotification();
        stopConnection();
    }

    @Override
    public synchronized void stopConnection() {
        if(isConnectionStarted){
            unregisterReceiver(mBluetoothStateReceiver);
        }

        if(mManager != null) mManager.stopAcceptingConnections();
        mManager = null;

        isConnectionStarted = false;
        setConnectionPreference(false);
    }

    @Override
    public boolean isConnectionStarted() {
        return isConnectionStarted;
    }

    @Override
    public boolean isConnected() {
        return mManager != null && mManager.isConnected();
    }

    private boolean isConnectionPreferenceOn(){
        return PreferenceManager.getDefaultSharedPreferences(getBaseContext())
                .getBoolean(PREF_CONNECTION_STARTED, false);
    }

    private void setConnectionPreference(boolean on){
        PreferenceManager.getDefaultSharedPreferences(getBaseContext())
                .edit()
                .putBoolean(PREF_CONNECTION_STARTED, on)
                .commit();
    }
}

