package com.duster.fr.feetmeinsole.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.duster.fr.feetmeinsole.R;
import com.duster.fr.feetmeinsole.activities.InsoleActivity;
import com.duster.fr.feetmeinsole.greendao.DaoAccess;
import com.duster.fr.feetmeinsole.greendao.DaoSession;
import com.duster.fr.feetmeinsole.greendao.Insole;
import com.duster.fr.feetmeinsole.greendao.InsoleDao;
import com.duster.fr.feetmeinsole.views.InsoleCreationView;

/**
 * Created by Anas on 29/02/2016.
 */
public class InsoleCreationFragment extends Fragment implements InsoleCreationView.Listener{



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_insole_creation, container, false);

        final InsoleCreationView insoleCreationView = (InsoleCreationView) view.findViewById(R.id.insole_view);
        insoleCreationView.setListener(this);

        TextView mAddInsoleBtn = (TextView) view.findViewById(R.id.add_insole);
        mAddInsoleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(insoleCreationView.areValuesEnteredCorrect()){
                    insoleCreationView.createInsole();
                }else{
                    Toast.makeText(getActivity().getApplicationContext(), R.string.incorrect_values, Toast.LENGTH_SHORT).show();
                }

            }
        });

        return view;
    }

    @Override
    public void onAddInsole(Insole insole) {
        DaoAccess access = DaoAccess.getInstance(getContext());
        DaoSession daoSession = access.openSession();

        InsoleDao insoleDao = daoSession.getInsoleDao();
        insoleDao.insert(insole);
        access.closeSession();

        startInsoleActivity();
    }

    private void startInsoleActivity() {
        getActivity().startActivity(InsoleActivity.newIntent(getActivity()));
        getActivity().supportFinishAfterTransition();
    }

}
