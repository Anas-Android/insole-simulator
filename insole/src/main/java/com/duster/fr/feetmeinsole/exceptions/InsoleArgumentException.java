package com.duster.fr.feetmeinsole.exceptions;

/**
 * Created by Anas on 24/02/2016.
 */
public class InsoleArgumentException extends IllegalArgumentException {

    public InsoleArgumentException(){}

    public InsoleArgumentException(String message){
        super(message);
    }
}