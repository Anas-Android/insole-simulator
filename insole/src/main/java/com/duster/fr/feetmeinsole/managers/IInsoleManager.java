package com.duster.fr.feetmeinsole.managers;

/**
 * Created by Anas on 19/02/2016.
 */
public interface IInsoleManager {

    /**
     * Broadcast intent ID for a remote device connection.
     */
    String ACTION_BT_CONNECTION = "fr.punchme.btconnection";

    /**
     * Broadcast intent ID for a remote device disconnection.
     */
    String ACTION_BT_DISCONNECTION = "fr.punchme.btdisconnection";

    /**
     * Broadcast intent ID for new remote device information.
     */
    String ACTION_REMOTE_DEVICE = "fr.punchme.remote.device";

    /**
     * Broadcast value of the side of the insole.
     */
    String EXTRA_SIDE = "side";

    /**
     * Broadcast name of the connected remote device.
     */
    String EXTRA_REMOTE_DEVICE_NAME = "fr.punchme.remote.device.name";

    /**
     * Broadcast name of the connected remote device.
     */
    String EXTRA_REMOTE_DEVICE_ADRESS = "fr.punchme.remote.device.adress";

    /**
     * Starts accepting connections from remote devices.
     */
    void startAcceptingConnections();

    /**
     * Stops accepting connections from remote devices.
     */
    void stopAcceptingConnections();

    /**
     * Gives the connection state to remote devices.
     */
    boolean isConnected();

}
