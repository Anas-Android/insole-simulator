package com.duster.fr.feetmeinsole.notifications;

/**
 * Created by Anas on 24/02/2016.
 */
public interface INotificationManager {

    int NOTIF_CODE_DISCONNECTION = 1;
    int NOTIF_CODE_BLUETOOTH = 2;

    void sendNotification();
}
