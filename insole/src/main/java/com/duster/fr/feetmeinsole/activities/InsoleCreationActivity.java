package com.duster.fr.feetmeinsole.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.duster.fr.feetmeinsole.R;
import com.duster.fr.feetmeinsole.fragments.InsoleCreationFragment;

/**
 * Created by Anas on 29/02/2016.
 */
public class InsoleCreationActivity extends ServiceActivity{

    public static Intent newIntent(Context packageContext){
        return new Intent(packageContext, InsoleCreationActivity.class);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insole_creation);

        Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_frame, new InsoleCreationFragment())
                .commit();
    }


}
