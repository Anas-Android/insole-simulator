package com.duster.fr.feetmeinsole.utils;

import com.duster.fr.feetmeinsole.exceptions.InsoleArgumentException;
import com.duster.fr.feetmeinsole.greendao.Insole;

/**
 * This class holds general information about the insole.
 *
 * Created by Anas on 24/02/2016.
 */
public class InsoleUtils {
    public static final int OVERHEAD = 5;
    public static final int SENSOR_NB_43 = 70;
    public static final int SENSOR_NB_40 = 68;
    public static final int SENSOR_NB_37 = 63;

    public static final int SIDE_LEFT = 1;
    public static final int SIDE_RIGHT = 2;
    public static final int MAX_SENSOR_VALUE = 224;


    /**
     * @param address MAC address of the insole.
     * @param name Name of the insole "FeetMe [number]-[size]"
     * @return A new insole instance.
     * @throws InsoleArgumentException
     */
    public static Insole newInstance(String address, String name){

        if(address == null){
            throw new InsoleArgumentException("address is null");
        }

        Insole insole = new Insole();
        insole.setAddress(address);
        insole.setName(name);
        insole.setSide(getSide(name));
        insole.setSize(getSize(name));
        //insole.setSensorNb(getSensorNb(insole.getSize()));

        return insole;
    }

    private static int getSide(String name) {
        int rIndex = name.indexOf('R');
        int lIndex = name.indexOf('L');

        if(rIndex == -1 && lIndex == -1){
            throw new InsoleArgumentException("No side in name");
        }else if(rIndex != -1 && lIndex != -1){
            return lIndex < rIndex ? SIDE_LEFT : SIDE_RIGHT;
        }else{
            return lIndex > rIndex ? SIDE_LEFT : SIDE_RIGHT;
        }
    }

    private static int getSize(String name) {
        int insoleSize = 0;
        if(name != null){
            String[] split = name.split("-");

            if(split.length == 1) return SENSOR_NB_43;

            insoleSize = Integer.valueOf(split[1]);
        }

        if(insoleSize < 37 || insoleSize > 45){
            throw new InsoleArgumentException("Size not in range");
        }

        return insoleSize;
    }
}
