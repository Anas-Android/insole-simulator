package com.duster.fr.feetmeinsole.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.duster.fr.feetmeinsole.R;
import com.duster.fr.feetmeinsole.fragments.SettingsFragment;

/**
 * Created by Anas on 13/02/2016.
 */
public class SettingsActivity extends AppCompatActivity{

    public static Intent newIntent(Context packageContext){
        return new Intent(packageContext, SettingsActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insole_creation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_frame, new SettingsFragment())
                .commit();
    }
}
