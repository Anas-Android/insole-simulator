package com.duster.fr.feetmeinsole.greendao;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

/**
 * Created by Anas on 20/02/2016.
 */
public class InsoleDaoGenerator {

    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(17, "com.duster.fr.feetmeinsole.greendao");
        schema.enableKeepSectionsByDefault();

        Entity insole = addInsole(schema);

        String workingDir = System.getProperty("user.dir");
        new DaoGenerator().generateAll(schema, workingDir + "/insole/src/main/java");
    }

    private static Entity addInsole(Schema schema) {
        Entity insole = schema.addEntity("Insole");
        insole.addIdProperty();
        insole.addStringProperty("name").notNull();
        insole.addStringProperty("address").notNull();
        insole.addIntProperty("side");
        insole.addIntProperty("size");
        insole.addIntProperty("sensorNb");
        insole.addStringProperty("version");
        insole.addStringProperty("versionCode");

        return insole;
    }

}
