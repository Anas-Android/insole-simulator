package com.duster.fr.feetmeinsole.gl;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

/**
 * Created by Anas on 19/02/2016.
 */
public class InsoleView extends GLSurfaceView {

    private InsoleRenderer mRenderer;

    public InsoleView(Context context) {
        super(context);
        init(context);
    }

    public InsoleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {

    }

    public InsoleRenderer getRenderer(){
        return mRenderer;
    }

}
