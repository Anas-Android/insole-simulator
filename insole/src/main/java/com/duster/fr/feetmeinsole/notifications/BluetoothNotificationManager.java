package com.duster.fr.feetmeinsole.notifications;

import android.content.Context;
import android.content.res.Resources;

/**
 * Created by Anas on 24/02/2016.
 */
public class BluetoothNotificationManager extends NotificationManager {

    public BluetoothNotificationManager(Context context) {
        super(context);
    }

    @Override
    public void sendNotification() {
        Resources resources = mContext.getResources();
        sendNotification("BT OFF","BT switched off", NOTIF_CODE_BLUETOOTH);
    }
}