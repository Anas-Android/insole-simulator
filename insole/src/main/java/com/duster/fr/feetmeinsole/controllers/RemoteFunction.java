package com.duster.fr.feetmeinsole.controllers;

/**
 * Created by Anas on 23/02/2016.
 */
public enum  RemoteFunction {
    MSG_START_SENDING_FRAMES(0,"msg_frame"),
    PONG(1, "pong"),
    SENSOR_NB(2,"sensors_number"),
    VERSION(3, "version"),
    SIDE(4, "insole_side"),
    RESET_TIMESTAMP(5, "reset_timestamp"),
    BATTERY(6, "battery_info"),
    SIZE(8, "footsize"),
    START_SENDING(10, "start_sending"),
    STOP_SENDING(11, "stop_sending"),
    MADM(14, "madm"),
    CALIBRATION(15, "sensor_calibration"),
    IGNORED(16,"ignored");

    private final int code;
    private final String message;

    RemoteFunction(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public final int code() {
        return code;
    }

    public final String message() {
        return message;
    }
}
