package com.duster.fr.feetmeinsole.controllers;

import android.support.annotation.Nullable;
import android.view.View;

import com.duster.fr.feetmeinsole.gl.InsoleRenderer;
import com.duster.fr.feetmeinsole.gl.InsoleView;
import com.duster.fr.feetmeinsole.greendao.Insole;

/**
 * Created by Anas on 19/02/2016.
 */
public class PressureMapController implements IPressureMapController {

    private static final String TAG = PressureMapController.class.getSimpleName();
    private static final int REFRESH_PERIOD = 100; //ms = 10 Hz

    private InsoleView mInsoleView;
    private InsoleRenderer mInsoleRenderer;
    private View mBackground;

    private Insole mInsole;

    public PressureMapController(InsoleView insoleView, @Nullable View bg){
        mInsoleView = insoleView;
        mInsoleRenderer = mInsoleView.getRenderer();
        mBackground = bg;
    }


    @Override
    public void onResume(){
        mInsoleView.onResume();
    }

    @Override
    public void onPause(){
        mInsoleView.onPause();
    }

    @Override
    public void startDrawing() {
        if(mBackground != null) {
            mBackground.setVisibility(View.INVISIBLE);
        }
        mInsoleRenderer.enableDrawing(mInsole);
        mInsoleView.setVisibility(View.VISIBLE);
        mInsoleView.requestRender();
    }

    @Override
    public void stopDrawing() {
        mInsoleRenderer.disableDrawing();
        mInsoleView.requestRender();
        mInsoleView.setVisibility(View.INVISIBLE);
        if(mBackground != null) {
            mBackground.setVisibility(View.VISIBLE);
        }

    }


    //TODO
}
