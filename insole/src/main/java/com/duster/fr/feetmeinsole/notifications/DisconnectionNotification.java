package com.duster.fr.feetmeinsole.notifications;

import android.content.Context;
import android.content.res.Resources;

/**
 * Created by Anas on 24/02/2016.
 */
public class DisconnectionNotification extends NotificationManager {

    private boolean isNotificationSent = false;

    public DisconnectionNotification(Context context) {
        super(context);
    }


    @Override
    public void sendNotification() {
        if(!isNotificationSent) {
            isNotificationSent = true;
            Resources resources = mContext.getResources();
            sendNotification("Remote device is disconnected",
                    "Remote device is disconnected", NOTIF_CODE_DISCONNECTION);
        }
    }

    public void prepareNotification(){
        isNotificationSent = false;
    }
}
