package com.duster.fr.feetmeinsole.data;

import android.support.annotation.NonNull;

/**
 * Created by Anas on 23/02/2016.
 */
public class Version implements Comparable<Version> {
    private final int major;
    private final int minor;
    private final int patch;

    public Version(int major, int minor, int patch){
        this.major = major;
        this.minor = minor;
        this.patch = patch;
    }

    /**
     * Parses a string to return a Version object.
     *
     * @return the Version object corresponding to the version string.
     * Null if the string can't be parsed.
     */
    public static Version fromString(String versionString){
        if(versionString == null) return null;

        String[] args = versionString.split("\\.");
        if (args.length != 3) return null;

        try{
            int major = Integer.parseInt(args[0]);
            int minor = Integer.parseInt(args[1]);
            int patch = Integer.parseInt(args[2]);

            if(major < 0 || minor < 0 || patch < 0) return null;

            return new Version(major, minor, patch);
        }catch (NumberFormatException e){
            return null;
        }
    }

    public int major() { return major; }

    public int minor() { return minor; }

    public int patch() { return patch; }

    @Override
    public String toString(){
        return major + "." + minor + "." + patch;
    }

    @Override
    public int compareTo(@NonNull Version version) {
        if(major < version.major) return -1;
        else if(major > version.major) return 1;

        if(minor < version.minor) return -1;
        else if(minor > version.minor) return 1;

        if(patch < version.patch) return -1;
        else if(patch > version.patch) return 1;
        else return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Version)) return false;

        Version version = (Version) o;
        return major == version.major
                && minor == version.minor
                && patch == version.patch;
    }
}
