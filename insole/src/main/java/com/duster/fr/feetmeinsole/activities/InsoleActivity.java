package com.duster.fr.feetmeinsole.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.duster.fr.feetmeinsole.R;
import com.duster.fr.feetmeinsole.fragments.InsoleFragment;

public class InsoleActivity extends ServiceActivity implements AppBarLayout.OnOffsetChangedListener{

    private static final int PERCENTAGE_TO_SHOW_IMAGE = 20;
    private View mAddFab;
    private int mMaxScrollSize;
    private boolean mIsImageHidden;

    public static Intent newIntent(Context packageContext){
        return new Intent(packageContext, InsoleActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insole);
        Toolbar toolbar = (Toolbar) findViewById(R.id.insole_toolbar);
        mAddFab = findViewById(R.id.insole_add_fab);

        mAddFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startInsoleCreationActivity();
            }
        });
        toolbar.setNavigationIcon(R.drawable.vector_drawable_left_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                previousActivity();
            }
        });

        AppBarLayout appbar = (AppBarLayout) findViewById(R.id.insole_appbar);
        appbar.addOnOffsetChangedListener(this);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.insole_layout, new InsoleFragment())
                .commit();

    }

    private void startInsoleCreationActivity() {
        startActivity(InsoleCreationActivity.newIntent(this));
    }

    private void previousActivity(){
        startActivity(HomeActivity.newIntent(this));
        overridePendingTransition(R.anim.fade_in, R.anim.fade_top_out);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (mMaxScrollSize == 0)
            mMaxScrollSize = appBarLayout.getTotalScrollRange();

        int currentScrollPercentage = (Math.abs(verticalOffset)) * 100
                / mMaxScrollSize;

        if (currentScrollPercentage >= PERCENTAGE_TO_SHOW_IMAGE) {
            if (!mIsImageHidden) {
                mIsImageHidden = true;

                ViewCompat.animate(mAddFab).scaleY(0).scaleX(0).start();
            }
        }

        if (currentScrollPercentage < PERCENTAGE_TO_SHOW_IMAGE) {
            if (mIsImageHidden) {
                mIsImageHidden = false;
                ViewCompat.animate(mAddFab).scaleY(1).scaleX(1).start();
            }
        }
    }
}
