package com.duster.fr.feetmeinsole.controllers;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Anas on 23/02/2016.
 */
public interface IRemoteFunctionController {

//    void startResponseDispatcherThread(OutputStream outputStream);
//    void stopResponseDispatcherThread();
    void onConnection() throws IOException;
    void onPingRequest() throws IOException;
    void onSensorNbRequest() throws IOException;
    void onVersionRequest() throws IOException;
    void onSideRequest() throws IOException;
    void onSizeRequest() throws IOException;
    void onBatteryLevelRequest() throws IOException;
    void onStartSendingRequest() throws IOException;
    void onStopSendingRequest() throws IOException;
    void onCalibrationRequest() throws IOException;
    void onResetTimeStampRequest() throws IOException;
}
