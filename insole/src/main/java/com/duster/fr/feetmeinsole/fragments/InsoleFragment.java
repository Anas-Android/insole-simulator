package com.duster.fr.feetmeinsole.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.duster.fr.feetmeinsole.R;
import com.duster.fr.feetmeinsole.adapters.InsoleAdapter;
import com.duster.fr.feetmeinsole.greendao.DaoAccess;
import com.duster.fr.feetmeinsole.greendao.DaoSession;
import com.duster.fr.feetmeinsole.greendao.Insole;
import com.duster.fr.feetmeinsole.greendao.InsoleDao;
import com.duster.fr.feetmeinsole.utils.PreferenceUtils;

import java.util.List;

/**
 * Created by Anas on 28/02/2016.
 */
public class InsoleFragment extends ServiceFragment {

    InsoleAdapter mInsoleAdapter;
    ListView mInsolesList;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_insole, container, false);

        mInsolesList= (ListView) view.findViewById(R.id.insoles_list);
        mInsoleAdapter = new InsoleAdapter(getActivity());
        mInsolesList.setAdapter(mInsoleAdapter);

        mInsolesList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                PreferenceUtils.clearInsole(getContext());
                PreferenceUtils.setDefaultInsole(getContext(), mInsoleAdapter.getItem(position));
                mInsoleAdapter.setDefaultInsole(mInsoleAdapter.getItem(position));
                mInsoleAdapter.sort();
                mInsoleAdapter.notifyDataSetChanged();
                return true;
            }
        });

        loadInsolesList();
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void loadInsolesList() {
        new AsyncTask<Void, Void, List<Insole>>() {

            Context context = getActivity().getApplicationContext();
            @Override
            protected List<Insole> doInBackground(Void... params) {

                DaoAccess access = DaoAccess.getInstance(context);
                DaoSession daoSession = access.openSession();
                InsoleDao insoleDao = daoSession.getInsoleDao();

                List<Insole> insoles = insoleDao.queryBuilder()
                        .list();
                access.closeSession();

                return insoles;
            }

            @Override
            protected void onPostExecute(List<Insole> insoles) {
                for(Insole insole: insoles) mInsoleAdapter.add(insole);

                mInsoleAdapter.setDefaultInsole(PreferenceUtils.getDefaultInsole(context));
                mInsoleAdapter.sort();
                mInsoleAdapter.notifyDataSetChanged();

                if(getActivity() != null){
                    updateListNoSort(mInsoleAdapter);
                }
            }
        }.execute();
    }

    protected void updateListNoSort(final InsoleAdapter adapter){
        if(mMainServiceManager != null){
            adapter.setInsoleConnected(mMainServiceManager.isConnected());
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onServiceConnected() {

    }
}
